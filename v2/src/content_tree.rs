use std::path::{Path, PathBuf};

use itertools::Itertools;

use serde_derive::{Serialize, Deserialize};
use toml;


#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct File {
    pub title: String,
    pub src: PathBuf,
    pub location: Option<String>,
    pub date: Option<String>
}

impl File {
    pub fn details_string(&self) -> Option<String> {
        let values = vec![
                self.location.as_ref(),
                self.date.as_ref(),
            ]
            .into_iter()
            .filter_map(|v| v)
            .collect::<Vec<_>>();
        if !values.is_empty() {
            Some(values.into_iter().join(", "))
        }
        else {
            None
        }
    }
}


#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct LeafDetails {
    pub name: String,
    pub description: Option<String>,
    pub files: Vec<File>
    // Extra GPS coordinate
}

impl LeafDetails {
    pub fn should_have_extra_page(&self) -> bool {
        self.description.is_some()
            || self.files.len() > 1
    }

    /**
      Returns the first file in the file list.
      Panics if no such file exists
    */
    pub fn main_file(&self) -> &File {
        self.files.first().expect("Flower did not have any associated files")
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct InnerDetails {
    pub name: String,
    pub order: Vec<String>,
}

#[derive(Debug, Clone)]
pub enum Node {
    Inner(InnerDetails, Vec<Node>),
    Leaf(PathBuf, LeafDetails)
}

impl Node {
    pub fn traverse<F, G, T>(
        &self,
        inner_fn: &F,
        leaf_fn: &G
    ) -> Vec<T>
        where F: Fn(&InnerDetails) -> T,
              G: Fn(&Path, &LeafDetails) -> T,
    {
        match self {
            Node::Inner(details, children) => {
                let mut result = vec!(inner_fn(
                    details
                ));
                for child in children {
                    result.append(
                        &mut child.traverse(inner_fn, leaf_fn)
                    )
                }
                result
            }
            Node::Leaf(file_system_name, details) => {
                vec!(leaf_fn(file_system_name, details))
            }
        }
    }
}




// Deserialization structs
#[derive(Deserialize, Serialize, Clone)]
pub struct LeafDeserializeable {
    pub leaf: LeafDetails
}
impl LeafDeserializeable {
    pub fn de(content: &str) -> Result<Self, toml::de::Error> {
        toml::from_str(content)
    }
    pub fn se(&self) -> Result<String, toml::ser::Error> {
        toml::to_string_pretty(self)
    }
}

#[derive(Deserialize, Clone)]
pub struct InnerDeserializeable {
    pub category: InnerDetails
}

impl InnerDeserializeable {
    pub fn de(content: &str) -> Result<Self, toml::de::Error> {
        toml::from_str(content)
    }
}


// Support structs for migrating from a previous struct format to a new one.  Replace the structure
// of these structs with the structure of the old struct, update the struct and then touch up the
// implementations for `into_non_legacy` where applicable.  The conversion happens automatically if
// the leaf nodes fails to load in ::traverse_dir
#[derive(Deserialize, Clone)]
pub struct LegacyLeafDeserializeable {
    pub leaf: LegacyLeafDetails
}
impl LegacyLeafDeserializeable {
    pub fn de(content: &str) -> Result<Self, toml::de::Error> {
        toml::from_str(content)
    }

    pub fn into_non_legacy(self) -> LeafDeserializeable{
        unimplemented!()
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct LegacyLeafDetails {
    pub name: String,
    pub filename: PathBuf,

    pub description: Option<String>,
    pub additional_files: Option<Vec<File>>
    // Extra GPS coordinate
}

impl LegacyLeafDetails {
    #[allow(dead_code)]
    pub fn into_non_legacy(self) -> LeafDetails {
        unimplemented!()
    }
}
