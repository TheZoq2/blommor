use crate::content_tree::{Node, LeafDetails, InnerDetails, File};

use std::path::{Path, PathBuf};

/// Converts a leaf node to HTML to put in the main page
fn leaf_to_html(details: &LeafDetails, path_to_dir: &Path) -> String {
    // TODO: Don't use lossy
    let base_dir = path_to_dir.to_string_lossy();

    let low_res_path = format!(
        "{}/low/{}",
        base_dir,
        details.main_file().src.to_string_lossy()
    );
    let high_res_path = format!(
        "{}/high/{}",
        base_dir,
        details.main_file().src.to_string_lossy()
    );
    let link_path = if details.should_have_extra_page() {
        let mut path = PathBuf::from(path_to_dir);
        path.push("details.html");
        path.to_string_lossy().into()
    }
    else {
        high_res_path
    };

    let title = format!(
        "{}{}",
        // Append a + if there is a subpage
        if details.files.len() > 1 {"+ "} else {""},
        details.name,
    );

    format!(r#"
        <div class="leaf">
            <a href={}><img src="{}"></a>
            <h6>{}</h6>
            {}
        </div>
        "#,
        link_path,
        low_res_path,
        title,
        details.main_file().details_string().as_ref()
            .map(|l| format!("<p>{}</p>", l))
            .unwrap_or("".into())
    )
}

/// Converts an inner node to HTML for the main page
fn inner_to_html(details: &InnerDetails, depth: u32) -> String {
    let header = match depth {
        0 => "h1",
        1 => "h2",
        2 => "h3",
        3 => "h4",
        _ => "h5",
    };

    format!("<{}>{}</{}>", header, details.name, header)
}

impl Node {
    fn to_html(&self, depth: u32) -> String {
        match self {
            Node::Leaf(path_to_dir, details) => {
                leaf_to_html(details, &path_to_dir)
            }
            Node::Inner(details, children) => {
                let mut result = inner_to_html(details, depth);
                result.push_str("\n<div class=\"child_container\">");
                for node in children {
                    result.push_str(&format!("\n{}", node.to_html(depth+1)))
                }
                result.push_str("\n</div>");
                result
            }
        }
    }
}


pub fn root_to_html(root: Node) -> String {
    match root {
        Node::Leaf(_, _) => panic!("Root node must be an inner node"),
        Node::Inner(InnerDetails{ref name, ..}, _) if name != "root" => {
            panic!("Root node must be called root")
        }
        Node::Inner(_, children) => {
            let mut result = "".to_string();
            for child in children {
                result.push_str(&child.to_html(0));
            }
            result
        }
    }
}




impl LeafDetails {
    pub fn extra_content(&self) -> Option<(String, String)> {
        if self.should_have_extra_page() {
            let ancestor_count = self.files.first().unwrap()
                .src.ancestors().count();

            let mut css_path = (0..ancestor_count+1).map(|_| "../")
                .collect::<String>();
            css_path += "css/style.css";
            let stylesheet = format!("<link rel=stylesheet href='{}'>", css_path);

            let header = format!("<h1>{}</h1>", self.name);

            let images = self.files
                .clone();

            let description = self.description.clone()
                .map(|d| format!("<div class='description'>{}</div>", d))
                .unwrap_or("".into());

            let image_tags = images.iter()
                .map(|f| {
                    let File {title, src, ..} = f;
                    format!(
                        r#"
                        <div class="leaf">
                            <a href=high/{}><img src="low/{}"></a>
                            <h6>{}</h6>
                            {}
                        </div>
                        "#,
                        src.to_string_lossy(),
                        src.to_string_lossy(),
                        title,
                        f.details_string().as_ref()
                            .map(|l| format!("<p>{}</p>", l))
                            .unwrap_or("".into())
                    )
                })
                .collect::<Vec<_>>();

            Some((
                format!("{}", stylesheet),
                format!(
                    "{}\n{}\n{}",
                    header,
                    description,
                    image_tags.join("")
                )
            ))
        }
        else {
            None
        }
    }
}
