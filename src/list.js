function createList()
{
    var mainDiv = document.getElementById("main");

    var finalList = "";
    
    for(var i = 0; i < mainDiv.children.length; i++)
    {
        var container = mainDiv.children[i];
        
        var innerDiv = container.firstChild;
        var isImg = false;
        if (container.className.search("imgContainer") != -1)
        {
            isImg = true;
        }

        if (isImg)
        {
            var name = "";

            for(var n = 0; n < innerDiv.children.length; n++)
            {
                var child = innerDiv.children[n];

                if(child.nodeName === "H2")
                {
                    name = child.innerHTML;
                }
            }
            var newTag = "<li><a href='#" + i + "'>" + name + "</a></li>";

            //Set the ID of the element
            container.id = "" + i;
            finalList = finalList + newTag;
        }
        else if(container.className.search("headerContainer") != -1)
        {
            var name = "";

            for(var n = 0; n < container.children.length; n++)
            {
                var child = container.children[n];

                if(child.nodeName === "H1")
                {
                    name = child.innerHTML;
                }
            }
            var newTag = "<li><a class=\"contentHeader\" href='#" + i + "'>" + name + "</a></li>";

            //Set the ID of the element
            container.id = "" + i;
            finalList = finalList + newTag;
        }
        else if(container.className.search("twoImagesContainer") != -1)
        {
            var name = "Dubbel";

            for(var n = 0; n < container.children.length; n++)
            {
                var child = container.children[n];

                for(var m = 0; m < child.children.length; m++)
                {
                    var childChild = child.children[m];
                    if(childChild.nodeName === "H2")
                    {
                        name = childChild.innerHTML;
                    }
                }
            }
            
            var newTag = "<li><a href='#" + i + "'>" + name + "</a></li>";

            //Set the ID of the element
            container.id = "" + i;
            finalList = finalList + newTag;
        }

    }

    document.getElementById("listContainer").innerHTML = finalList;
}
